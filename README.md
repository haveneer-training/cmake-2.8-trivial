# README

## This is a sample project to explain gitlab features

CI Config includes:
* CMake/C++ in alpine distribution
* multiple compilation stages
* multiple test stages
* Dummy static analysis
* Artifact production and storage 